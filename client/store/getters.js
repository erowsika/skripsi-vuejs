const pkg = state => state.pkg
const app = state => state.app
const device = state => state.app.device
const sidebar = state => state.app.sidebar
const effect = state => state.app.effect
const idUser = state => state.idUser
const roles = state => state.roles
const getUsername = state => state.username

export {
  pkg,
  app,
  device,
  sidebar,
  effect,
  idUser,
  roles,
  getUsername
}
