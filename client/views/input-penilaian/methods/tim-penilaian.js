import Vue from 'vue'
import VueEvents from 'vue-events'
import CustomActions from '../CustomActions'
import { TIM_PENILAIAN_URL } from 'config-api'
import Notification from 'vue-bulma-notification'

Vue.use(VueEvents)

Vue.component('custom-actions', CustomActions)

const NotificationComponent = Vue.extend(Notification)

const openNotification = (propsData = {
    title: '',
    message: '',
    type: '',
    direction: '',
    duration: 4500,
    container: '.notifications'
}) => {
    return new NotificationComponent({
        el: document.createElement('div'),
        propsData
    })
}

export function closeModalBasic() {
    this.showModal = false
}

export function onPaginationData(paginationData) {
    this.$refs.pagination.setPaginationData(paginationData)
    this.$refs.paginationInfo.setPaginationData(paginationData)
}

export function onChangePage(page) {
    this.$refs.vuetable.changePage(page)
}

export function onCellClicked(data, field, event) {
    this.$refs.vuetable.toggleDetailRow(data.id)
}

export function openNotificationWithType(type, name, newTitle) {
    openNotification({
        title: newTitle,
        message: 'Kriteria :\n' + name,
        type: type
    })
}

export function eventActions(type, value) {
    switch (type) {
        case 'edit':
            {
                this.$router.push({
                    path: '/topsis/kriteria/edit/' + value.id,
                    params: value.id
                })
                break
            }
        case 'add-success':
            {
                this.openNotificationWithType('success', value, 'Success Add New Kriteria')
                break
            }
        case 'edit-success':
            {
                this.openNotificationWithType('success', value, 'Success Update Kriteria')
                break
            }
        case 'filter-set':
            {
                this.moreParams = {
                    'filter': value
                }
                this.$nextTick(() => this.$refs.vuetable.refresh())
                break
            }
        case 'filter-reset':
            {
                this.moreParams = {}
                this.$nextTick(() => this.$refs.vuetable.refresh())
                break
            }
        default:
            break
    }
}

export function modalActions(type, value) {
    switch (type) {
        case 'modal-show':
            {
                this.showModal = true
                this.dataTimPenilain = value
                break
            }
        case 'close-delete':
            this.showModal = false
            break
        case 'yes-delete':
            this.axios.delete(`${TIM_PENILAIAN_URL}/${this.dataTimPenilain.id}`)
                .then(res => {
                    this.$nextTick(() => this.$refs.vuetable.refresh())
                    this.showModal = false
                    this.openNotificationWithType('success', this.dataTimPenilain.name, 'Succes Delete')
                })
                .catch(err => {
                    console.log('err ', err)
                    this.showModal = false
                    this.openNotificationWithType('danger', this.dataTimPenilain.name, 'Failed Delete')
                })
            break
        default:
            break
    }
}

export function rolesTransformer(value, fmt) {
    if (value == null) return ''
    return value.map(el => {
        return el.role
    }).join(', ')

}