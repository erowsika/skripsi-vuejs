<div align="center">
<p><img width="200" src="https://github.com/vue-bulma/vue-admin/blob/master/client/assets/logo@2x.png"></p>

<h1>Vue Admin App</h1>

<p>
  <strong>Based on</strong>
  <a href="https://github.com/vue-bulma/vue-admin">Vue Admin</a>
</p>
</div>

## Features
* Powered by [Vue][] **2.5** & [Bulma][] **0.3**
* Responsive and Flexible Box Layout

## [Development](doc/development.md)

### Requirements

  * Node >= v5

  * NPM >= v3

  * Webpack v2

[Vue]: http://vuejs.org
[Bulma]: http://bulma.io
[Vue Admin]: https://github.com/vue-bulma/vue-admin