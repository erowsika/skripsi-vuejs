import { PEGAWAI_URL } from 'config/api'

export const pegawaiUrl = PEGAWAI_URL

export const loading = false

export const css = {
  tableClass: 'table is-bordered is-striped',
  ascendingIcon: 'fa fa-chevron-up',
  descendingIcon: 'fa fa-chevron-down',
  sortHandleIcon: 'fa fa-bars'
}

export const perPage = '10'

export const fields = [{
  name: 'id',
  visible: false
},
{
  name: 'name',
  title: 'Nama',
  sortField: 'name'
},
{
  name: 'nip',
  title: 'NIP',
  sortField: 'nip'
},
{
  name: 'pangkat',
  title: 'Pangkat',
  sortField: 'pangkat'
},
{
  name: 'golongan',
  title: 'Golongan',
  sortField: 'golongan'
},
{
  name: 'jabatan',
  title: 'Jabatan',
  sortField: 'jabatan'
},
{
  name: '__component:custom-actions-pegawai',
  title: 'Actions',
  titleClass: 'has-text-centered',
  dataClass: 'has-text-centered'
}
]

export const sortOrder = [{
  field: 'username',
  sortField: 'id',
  direction: 'asc'
}]

export const moreParams = {}

export const dataPegawai = {}

export const showModal = false
