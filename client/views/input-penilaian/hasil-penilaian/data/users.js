import { WAKTU_PENILAIAN_TOPSIS_URL } from 'config-api'

export const waktuPenilaianTopsisUrl = WAKTU_PENILAIAN_TOPSIS_URL

export const loading = false

export const css = {
    tableClass: 'table is-bordered is-striped',
    ascendingIcon: 'fa fa-chevron-up',
    descendingIcon: 'fa fa-chevron-down',
    sortHandleIcon: 'fa fa-bars'
}

export const perPage = '10'

export const fields = [{
        name: 'id',
        visible: false

    },
    {
        name: 'tanggal',
        title: 'Tanggal',
        sortField: 'tanggal'
    },
    {
        name: '__component:custom-actions',
        title: 'Actions',
        titleClass: 'has-text-centered',
        dataClass: 'has-text-centered'
    }
]

export const sortOrder = [{
    field: 'tanggal',
    sortField: 'tanggal',
    direction: 'desc'
}]

export const moreParams = {}

export const dataUser = {}

export const showModal = false