import { KRITERIA_URL } from 'config/api'

export const kriteriaUrl = KRITERIA_URL

export const loading = false

export const css = {
  tableClass: 'table is-bordered is-striped',
  ascendingIcon: 'fa fa-chevron-up',
  descendingIcon: 'fa fa-chevron-down',
  sortHandleIcon: 'fa fa-bars'
}

export const perPage = '10'

export const fields = [{
  name: 'id',
  visible: false
},
{
  name: 'name',
  title: 'Nama Kriteria',
  sortField: 'name'
},
{
  name: 'bobot',
  title: 'Bobot',
  sortField: 'bobot'
},
{
  name: '__component:custom-actions-kriteria',
  title: 'Actions',
  titleClass: 'has-text-centered',
  dataClass: 'has-text-centered'
}
]

export const sortOrder = [{
  field: 'name',
  sortField: 'id',
  direction: 'asc'
}]

export const moreParams = {}

export const dataUser = {}

export const showModal = false
