import Vue from 'vue'
import VueEvents from 'vue-events'
import Notification from 'vue-bulma-notification'
import { WAKTU_PENILAIAN_TOPSIS_URL, IS_WAKTU_PENILAIAN_HASIL_PENILAIAN_URL } from 'config/api'
import CustomActionsTopsis from '../CustomActionsTopsis'
import CustomActionsPromethee from '../CustomActionsPromethee'

Vue.use(VueEvents)

Vue.component('custom-actions-topsis', CustomActionsTopsis)
Vue.component('custom-actions-promethee', CustomActionsPromethee)

const NotificationComponent = Vue.extend(Notification)

const openNotification = (propsData = {
  title: '',
  message: '',
  type: '',
  direction: '',
  duration: 4500,
  container: '.notifications'
}) => {
  return new NotificationComponent({
    el: document.createElement('div'),
    propsData
  })
}

export function closeModalBasic () {
  this.showModal = false
}

export function onPaginationData (paginationData) {
  this.$refs.pagination.setPaginationData(paginationData)
  this.$refs.paginationInfo.setPaginationData(paginationData)
}

export function onChangePage (page) {
  this.$refs.vuetable.changePage(page)
}

export function onCellClicked (data, field, event) {
  this.$refs.vuetable.toggleDetailRow(data.id)
}

export function openNotificationWithType (type, username, newTitle) {
  openNotification({
    title: newTitle,
    message: '',
    type: type
  })
}

export function eventActions (type, value) {
  switch (type) {
    case 'show-topsis':
      {
        this.$router.push({
          path: '/hasil_penilaian/topsis/' + value.id,
          params: { id: value.id }
        })
        break
      }
    case 'show-promethee':
      {
        this.axios.get(`${IS_WAKTU_PENILAIAN_HASIL_PENILAIAN_URL}/${value.id}`)
        .then(res => {
          this.$router.push({
            path: '/hasil_penilaian/promethee/' + value.id,
            params: { id: value.id }
          })
        })
        .catch(err => {
          this.openNotificationWithType('danger', value, 'Methode Promethee Not Submited')
          throw err
        })
        break
      }
    case 'add-success':
      {
        this.openNotificationWithType('success', value, 'Success Add New User')
        break
      }
    case 'edit-success':
      {
        this.openNotificationWithType('success', value, 'Success Update User')
        break
      }
    case 'filter-set':
      {
        this.moreParams = {
          'filter': value
        }
        this.$nextTick(() => this.$refs.vuetable.refresh())
        break
      }
    case 'filter-reset':
      {
        this.moreParams = {}
        this.$nextTick(() => this.$refs.vuetable.refresh())
        break
      }
    default:
      break
  }
}

export function modalActions (type, value) {
  switch (type) {
    case 'modal-show':
      {
        this.showModal = true
        this.dataUser = value
        break
      }
    case 'close-delete':
      this.showModal = false
      break
    case 'yes-delete':
      this.axios.delete(`${WAKTU_PENILAIAN_TOPSIS_URL}/${this.dataUser.id}`)
        .then(res => {
          this.$nextTick(() => this.$refs.vuetable.refresh())
          this.showModal = false
          this.openNotificationWithType('success', this.dataUser.username, 'Succes Delete User')
        })
        .catch(err => {
          console.log('err ', err)
          this.showModal = false
          this.openNotificationWithType('danger', this.dataUser.username, 'Failed Delete User')
        })
      break
    default:
      break
  }
}

export function rolesTransformer (value, fmt) {
  if (value == null) return ''
  return value.map(el => {
    return el.role
  }).join(', ')
}
