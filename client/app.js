import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import store from './store'
import router from './router'
import VueAxios from 'vue-axios'
import * as filters from './filters'
import NProgress from 'vue-nprogress'
import { sync } from 'vuex-router-sync'
import VueAuth from '@websanova/vue-auth'
import { LOGIN_URL } from 'config/api.js'
import { TOGGLE_SIDEBAR } from 'vuex-store/mutation-types'

// Enable devtools
Vue.config.devtools = true

Vue.router = router
Vue.use(VueAxios, axios)
Vue.use(VueAuth, {
  auth: {
    request: function (req, token) {
      this.options.http._setHeaders.call(this, req, {Authorization: 'Bearer ' + token})
    },
    response: function (res) {
      // Get Token from response body
      return res.data.token
    }
  },
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  loginData: { url: LOGIN_URL, method: 'POST', redirect: '/users', fetchUser: false },
  refreshData: { enabled: false },
  fetchUser: false
})

Vue.use(NProgress)

sync(store, router)

const nprogress = new NProgress({ parent: '.nprogress-container' })

const { state } = store

router.beforeEach((route, redirect, next) => {
  if (state.app.device.isMobile && state.app.sidebar.opened) {
    store.commit(TOGGLE_SIDEBAR, false)
  }
  next()
})

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

const app = new Vue({
  router,
  store,
  nprogress,
  ...App
})

export { app, router, store }
