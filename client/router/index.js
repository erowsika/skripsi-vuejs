import Vue from 'vue'
import Router from 'vue-router'

import Index from 'views/common/index.vue'

import ListUser from 'views/user/Users.vue'
import AddUser from 'views/user/AddUser.vue'
import EditUser from 'views/user/EditUser.vue'

import ListPegawai from 'views/pegawai/Pegawai.vue'
import AddPegawai from 'views/pegawai/AddPegawai.vue'
import EditPegawai from 'views/pegawai/EditPegawai.vue'

import Kriteria from 'views/kriteria/Kriteria.vue'
import AddKriteria from 'views/kriteria/AddKriteria.vue'
import EditKriteria from 'views/kriteria/EditKriteria.vue'

import HasilPenilaian from 'views/hasil-penilaian/HasilPenilaian.vue'
import InputPenilaian from 'views/input-penilaian/InputPenilaian.vue'
import ShowHasilTopsis from 'views/hasil-penilaian/ShowHasilTopsis.vue'
import ShowHasilPromethee from 'views/hasil-penilaian/ShowHasilPromethee.vue'

Vue.use(Router)

export default new Router({
  mode: 'history', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'is-active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [{
    name: 'Home',
    path: '/',
    component: require('../views/Home')
  },
  {
    name: 'Login',
    path: '/login',
    component: require('../views/Login')
  },
  {
    name: 'User',
    path: '/user',
    component: Index,
    children: [{
      path: '',
      component: ListUser
    },
    {
      name: 'Add User',
      path: 'add',
      component: AddUser
    },
    {
      name: 'Edit User',
      path: 'edit/:id',
      component: EditUser,
      props: (route) => {
        return { userId: parseInt(route.params.id) }
      }
    }
    ]
  },
  {
    name: 'Pegawai',
    path: '/pegawai',
    component: Index,
    children: [{
      path: '',
      component: ListPegawai
    },
    {
      name: 'Add Pegawai',
      path: 'add',
      component: AddPegawai
    },
    {
      name: 'Edit Pegawai',
      path: 'edit/:id',
      component: EditPegawai,
      props: (route) => {
        return { userId: parseInt(route.params.id) }
      }
    }
    ]
  },
  {
    name: 'Kriteria',
    path: '/kriteria',
    component: Index,
    children: [{
      path: '',
      component: Kriteria
    },
    {
      name: 'Add Kriteria',
      path: 'add',
      component: AddKriteria
    },
    {
      name: 'Edit Kriteria',
      path: 'edit/:id',
      component: EditKriteria,
      props: (route) => {
        return { kriteriaId: parseInt(route.params.id) }
      }
    }
    ]
  },
  {
    name: 'Input Penilaian',
    path: '/input_penilaian',
    component: Index,
    children: [{
      path: '',
      component: InputPenilaian
    }]
  },
  {
    name: 'Hasil Penilaian',
    path: '/hasil_penilaian',
    component: Index,
    children: [{
      path: '',
      component: HasilPenilaian
    },
    {
      name: 'Topsis',
      path: 'topsis',
      component: Index,
      children: [{
        path: '',
        redirect: '/hasil_penilaian'
      },
      {
        name: 'Hasil Topsis',
        path: ':id',
        component: ShowHasilTopsis,
        props: (route) => ({ waktuPenilaianId: parseInt(route.params.id) })

      }
      ]
    },
    {
      name: 'Promethee',
      path: 'promethee',
      component: Index,
      children: [{
        path: '',
        redirect: '/hasil_penilaian'
      },
      {
        name: 'Hasil Promethee',
        path: ':id',
        component: ShowHasilPromethee,
        props: route => ({ waktuPenilaianId: parseInt(route.params.id) })
      }
      ]
    }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
  ]
})
