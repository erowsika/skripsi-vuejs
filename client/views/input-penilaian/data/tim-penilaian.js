import { TIM_PENILAIAN_URL } from 'config-api'

export const timPenilaianUrl = TIM_PENILAIAN_URL

export const loading = false

export const css = {
  tableClass: 'table is-bordered is-striped',
  ascendingIcon: 'fa fa-chevron-up',
  descendingIcon: 'fa fa-chevron-down',
  sortHandleIcon: 'fa fa-bars'
}

export const perPage = '10'

export const fields = [
  // {
  //   name: '__checkbox',
  //   title: '#',
  //   titleClass: 'has-text-centered',
  //   dataClass: 'has-text-centered'
  // },
  {
    name: 'name',
    title: 'Nama',
    sortField: 'name'
  },
  {
    name: 'nip',
    title: 'NIP',
    sortField: 'nip'
  },
  {
    name: '__component:custom-actions',
    title: 'Actions',
    titleClass: 'has-text-centered',
    dataClass: 'has-text-centered'
  }
]

export const sortOrder = [
  {
    field: 'name',
    sortField: 'name',
    direction: 'asc'
  }
]

export const moreParams = {}

export const dataUser = {}

export const dataTimPenilain = {}

export const showModal = false
