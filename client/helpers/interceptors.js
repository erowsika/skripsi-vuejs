import Vue from 'vue'
import axios from 'axios'
import store from '../store'

export default function setup () {
  axios.interceptors.response.use(response => response, err => {
    // Do
    console.log('interseptors = ', Vue)
    Vue.auth.logout()
    store.commit('logout')
    console.log('erorr =', err)
    console.log('must logout')
    return Promise.reject(err)
  })
}
