import Vue from 'vue'
import Vuex from 'vuex'
import pkg from 'package'
import * as actions from './actions'
import * as getters from './getters'

import app from './modules/app'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  actions,
  getters,
  modules: {
    app
  },
  state: {
    authenticated: false,
    idUser: '',
    username: '',
    roles: '',
    token: '',
    pkg
  },
  mutations: {
    authenticated (state, payload) {
      state.authenticated = true
      state.idUser = payload.id
      state.username = payload.username
      state.roles = payload.roles
      state.token = payload.token
    },
    logout (state) {
      state.authenticated = false
      state.token = null
    }
  },
  plugins: [createPersistedState({
    key: 'skripsi-app',
    paths: [
      'idUser',
      'username',
      'roles',
      'token',
      'authenticated'
    ]
  })]
})

export default store
