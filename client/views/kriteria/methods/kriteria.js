import Vue from 'vue'
import VueEvents from 'vue-events'
import { KRITERIA_URL } from 'config/api'
import CustomActions from '../CustomActions'
import Notification from 'vue-bulma-notification'

Vue.use(VueEvents)

Vue.component('custom-actions-kriteria', CustomActions)

const NotificationComponent = Vue.extend(Notification)

const openNotification = (propsData = {
  title: '',
  message: '',
  type: '',
  direction: '',
  duration: 4500,
  container: '.notifications'
}) => {
  return new NotificationComponent({
    el: document.createElement('div'),
    propsData
  })
}

export function closeModalBasic () {
  this.showModal = false
}

export function onPaginationData (paginationData) {
  this.$refs.pagination.setPaginationData(paginationData)
  this.$refs.paginationInfo.setPaginationData(paginationData)
}

export function onChangePage (page) {
  this.$refs.vuetable.changePage(page)
}

export function onCellClicked (data, field, event) {
  this.$refs.vuetable.toggleDetailRow(data.id)
}

export function openNotificationWithType (type, kriteria, newTitle) {
  openNotification({
    title: newTitle,
    message: 'Kriteria :\n' + kriteria,
    type: type
  })
}

export function eventActions (type, value) {
  switch (type) {
    case 'edit':
      {
        this.$router.push({
          path: '/kriteria/edit/' + value.id,
          params: value.id
        })
        break
      }
    case 'add-success':
      {
        this.openNotificationWithType('success', value, 'Success Add New Kriteria')
        break
      }
    case 'edit-success':
      {
        this.openNotificationWithType('success', value, 'Success Update Kriteria')
        break
      }
    case 'filter-set':
      {
        this.moreParams = {
          'filter': value
        }
        this.$nextTick(() => this.$refs.vuetable.refresh())
        break
      }
    case 'filter-reset':
      {
        this.moreParams = {}
        this.sortOrder[0].sortField = 'id'
        this.$nextTick(() => this.$refs.vuetable.refresh())
        break
      }
    default:
      break
  }
}

export function modalActions (type, value) {
  switch (type) {
    case 'modal-show':
      {
        this.showModal = true
        this.dataUser = value
        break
      }
    case 'close-delete':
      this.showModal = false
      break
    case 'yes-delete':
      this.axios.delete(`${KRITERIA_URL}/${this.dataUser.id}`)
        .then(res => {
          this.$nextTick(() => this.$refs.vuetable.refresh())
          this.showModal = false
          this.openNotificationWithType('success', this.dataUser.name, 'Succes Delete Kriteria')
        })
        .catch(err => {
          console.log('err ', err)
          this.showModal = false
          this.openNotificationWithType('danger', this.dataUser.name, 'Failed Delete Kriteria')
        })
      break
    default:
      break
  }
}

export function rolesTransformer (value, fmt) {
  if (value == null) return ''
  return value.map(el => {
    return el.role
  }).join(', ')
}
