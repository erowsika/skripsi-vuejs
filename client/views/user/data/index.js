import { USER_URL } from 'config/api'

export const userUrl = USER_URL

export const loading = false

export const css = {
  tableClass: 'table is-bordered is-striped',
  ascendingIcon: 'fa fa-chevron-up',
  descendingIcon: 'fa fa-chevron-down',
  sortHandleIcon: 'fa fa-bars'
}

export const perPage = '10'

export const fields = [
  {
    name: 'id',
    visible: false
  },
  {
    name: 'username',
    title: 'Username',
    sortField: 'username'
  },
  {
    name: 'roles',
    title: 'Roles',
    callback: 'rolesTransformer'
  },
  {
    name: '__component:custom-actions-user',
    title: 'Actions',
    titleClass: 'has-text-centered',
    dataClass: 'has-text-centered'
  }
]

export const sortOrder = [{
  field: 'username',
  sortField: 'id',
  direction: 'asc'
}]

export const moreParams = {}

export const dataUser = {}

export const showModal = false
