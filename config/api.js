const LOCAL_URL = 'http://localhost:8080/'
// const SERVER_URL = 'https://www.sig2013.online/api/'
const SERVER_URL = 'http://192.168.2.111/lumen/'
let BASE_URL = ''

if (process.env.NODE_ENV) {
  switch (process.env.NODE_ENV) {
    case 'production':
      BASE_URL = SERVER_URL
      break

    default:
      BASE_URL = LOCAL_URL
      break
  }
}

export const LOGIN_URL = `${BASE_URL}login`

export const USER_URL = `${BASE_URL}user`

export const PEGAWAI_URL = `${BASE_URL}pegawai`

export const KRITERIA_URL = `${BASE_URL}kriteria`

export const TOPSIS_URL = `${BASE_URL}topsis`

export const WAKTU_PENILAIAN_TOPSIS_URL = `${BASE_URL}waktu_penilaian_topsis`

export const WAKTU_PENILAIAN_PROMETHEE_URL = `${BASE_URL}waktu_penilaian_promethee`

export const HASIL_PENILAIAN_URL = `${BASE_URL}hasil_penilaian`

export const WAKTU_PENILAIAN_HASIL_PENILAIAN_URL = `${BASE_URL}waktu_penilaian_hasil_penilaian`

export const IS_WAKTU_PENILAIAN_HASIL_PENILAIAN_URL = `${BASE_URL}is_waktu_penilaian_promethee`
